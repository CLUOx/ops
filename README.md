# OpenFMB Operational Use Case Data Model

This repository contains the OpenFMB operational use-case data models drawing upon IEC 61850 and CIM artifacts.

## Viewing the model

The UML model was built using Enterprise Architect (EA) from Sparx Systems. If you currently do not own a license for EA, there is a free viewer from Sparx Systems that you can download to view there model here:

[Enterprise Architect Viewer](https://www.sparxsystems.com/bin/EALite.msi)

## Copyright

See the COPYRIGHT file for copyright information of information contained in this repository.

## License

Unless otherwise noted, all files in this repository are distributed under the Apache Version 2.0 license found in the LICENSE file.
